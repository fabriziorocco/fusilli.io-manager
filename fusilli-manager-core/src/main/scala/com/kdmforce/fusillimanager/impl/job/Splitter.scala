package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{ConfigValue, Job}
import com.kdmforce.fusillimanager.impl.MessageSerializer.{JobMessage, LogMessage, PipelineAck, PipelineEnd, PipelineError, PipelineStart}


object Splitter {
  def props = Props[Splitter]
}

class Splitter(instance: Int, job: Job, follower: ActorRef ) extends Actor {
  var index = 0
  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
    )

  override def receive = {
    case PipelineStart =>
      log.info("START SPLITTER")
      logger ! (LogMessage(instance, JobMessage("START", "", job.job_configuration_id)))
      follower ! PipelineStart

    case PipelineEnd =>
      log.info("END SPLITTER")
      logger ! (LogMessage(instance, JobMessage("END", "", job.job_configuration_id)))
      follower ! PipelineEnd
      self ! PoisonPill


    case PipelineError(ex) =>
      log.error(ex, "ERROR ON SPLITTER")
      logger ! (LogMessage(instance, JobMessage("ERROR", ex.getMessage, job.job_configuration_id)))
      follower ! PipelineError(ex)
      self ! PoisonPill

    case el: Map[String, String] =>
      val result = splitFun(el, job.job_configs)
      index = index + 1
      log.info("SPLITTER OF " + s"${index}" + " ELEMENT")
      //logger ! (LogMessage(instance, JobMessage("SPLITTER RESULT: " + s"${result}", "", job.job_configuration_id)))
      follower ! result
  }

  def splitFun(doc: Map[String,String], jobConfigs: Seq[ConfigValue]): Map[String,String] = {
    var newDoc = doc
    for (j<- jobConfigs) {
      val newValues = newDoc(j.input_attrs(0)).split((j.config \ "spacing").as[String], 2)
      newDoc = newDoc - j.input_attrs(0) + (j.output_attrs(0) -> newValues(0)) + (j.output_attrs(1) -> newValues(1))
    }
    newDoc
  }
}
