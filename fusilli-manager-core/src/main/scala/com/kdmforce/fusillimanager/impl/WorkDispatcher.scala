package com.kdmforce.fusillimanager.impl

import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Pipeline, PipelineStop}
import com.kdmforce.fusillimanager.impl.Logger.Logger
import com.kdmforce.fusillimanager.impl.discovery.TheOtherOneAfterDispatcher
import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.event.Logging
import akka.Done
import akka.cluster.singleton.{ClusterSingletonManager, ClusterSingletonManagerSettings, ClusterSingletonProxy, ClusterSingletonProxySettings}
import com.kdmforce.fusillimanager.impl.MessageSerializer.{LogInstanceData, NewInstanceLog}

import com.kdmforce.fusillimanager.impl.MessageSerializer.{LogInstanceData, NewInstanceLog}
//import com.kdmforce.fusillimanager.impl.MessageSerializer.{NewDiscoveryLog, LogAutoDiscoveryData}
import com.kdmforce.fusillimanager.api.DiscoveryEndpointSerializer.DiscoveryDoc


object WorkDispatcher {
  def props = Props[WorkDispatcher]
}

class WorkDispatcher extends Actor {
  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonManager.props(
      singletonProps = Props(classOf[Logger]),
      terminationMessage = PoisonPill,
      settings = ClusterSingletonManagerSettings(context.system)),
    name = "logger")

  val loggerProxy = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system))
    )

  var otherOnesMap = Map[Int,ActorRef]()

  override def receive = {
    case request: Pipeline =>
      val theOneAfterDispatcher =
        context.system.actorOf(Props(classOf[TheOneAfterDispatcher], request.instance_id))

      otherOnesMap+= request.pipeline_id -> theOneAfterDispatcher

      loggerProxy ! NewInstanceLog(request.instance_id, LogInstanceData(request.logger, request.policies_id, request.pipeline_id))
      theOneAfterDispatcher ! request
      sender() ! Done

    case request: DiscoveryDoc =>
      val theOtherOneAfterDispatcher =
        context.system.actorOf(Props(classOf[TheOtherOneAfterDispatcher], request.autodiscovery_id))
      //loggerProxy ! NewDiscoveryLog(request.autodiscovery_id, LogAutoDiscoveryData(request.url_data_entry, request.repository.repo_id))
      theOtherOneAfterDispatcher ! request
      sender() ! Done

    case request: PipelineStop =>
      val stopId = request.pipeline_id
      val stoppingOneAfterDispatcher = otherOnesMap (stopId)
      //loggerProxy ! NewDiscoveryLog(request.autodiscovery_id, LogAutoDiscoveryData(request.url_data_entry, request.repository.repo_id))
      stoppingOneAfterDispatcher ! new PipelineStop (stopId)
      sender() ! Done
  }
}
