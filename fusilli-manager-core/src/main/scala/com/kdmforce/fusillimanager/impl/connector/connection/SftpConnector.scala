package com.kdmforce.fusillimanager.impl.connector.connection

import java.net.InetAddress

import akka.stream.IOResult
import akka.stream.alpakka.ftp.scaladsl.Sftp
import akka.stream.alpakka.ftp.{FtpCredentials, SftpSettings}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Future


object SftpConnector extends Connector[ByteString]{

  private final val log: Logger = LoggerFactory.getLogger("SFTP")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")
    val file = repo.schema_path + entity.entity_name


    log.info("START CONNECTION")
    val settings = spawnConnection(repo)

    log.info(s"FILE CONNECTION ${file}")
    Sftp.fromPath(file, settings)
  }

  override def getSink(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSINK")
    val file = repo.schema_path + entity.entity_name

    log.info("START CONNECTION")
    val settings = spawnConnection(repo)

    log.info(s"FILE CONNECTION ${file}")
    Sftp.toPath(file, settings)

  }


  def spawnConnection (repo: Repo): SftpSettings = {
    val credentials = FtpCredentials.create(repo.repo_user, repo.repo_password)

    SftpSettings
      .create(InetAddress.getByName(repo.host))
      .withPort(repo.port)
      .withCredentials(credentials)
      .withStrictHostKeyChecking(false)
  }
}
