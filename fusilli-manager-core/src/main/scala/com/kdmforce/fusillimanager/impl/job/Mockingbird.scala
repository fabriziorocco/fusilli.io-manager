package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Job
import com.kdmforce.fusillimanager.impl.MessageSerializer._

object Mockingbird {
  def props = Props[Mockingbird]
}

class Mockingbird(instance: Int, job: Job, predecessors:Int, followers: Seq[ActorRef] ) extends Actor {
  var index = 0
  var prevs = predecessors

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case PipelineStart =>
      log.info("START MOCKINGBIRD")
      logger ! (LogMessage(instance, JobMessage("START", "", job.job_configuration_id)))
      for (follower <- followers) { follower ! PipelineStart }

    case PipelineEnd =>
      prevs -= 1
      if (prevs == 0){
          log.info("END MOCKINGBIRD")
          logger ! (LogMessage(instance, JobMessage("END", "", job.job_configuration_id)))
          for (follower <- followers) { follower ! PipelineEnd }
          self ! PoisonPill
          }
      else  log.info("MOCKINBIRD RECEIVED A STOP," + prevs + " MORE NEEDED")
        


    case PipelineError(ex) =>
      log.error(ex, "ERROR ON MOCKINGBIRD")
      logger ! (LogMessage(instance, JobMessage("ERROR", ex.getMessage, job.job_configuration_id)))
      for (follower <- followers){ follower ! PipelineError(ex) }
      self ! PoisonPill

    case el: Map[String, String] =>
      index = index + 1
      log.info("MOCKINGBIRD OF " + s"${index}" + " ELEMENT")
      //log.info("WICH IS " + s"${el}" +"\n\n\n\n")
      //logger ! (LogMessage(instance, JobMessage("MOCKINGBIRD RESULT: " + s"${el}", "", job.job_configuration_id)))

      for (follower <- followers){ follower ! el }
  }

}
