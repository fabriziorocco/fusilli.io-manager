package com.kdmforce.fusillimanager.impl

import akka.actor.Actor
import akka.actor.Props
import akka.event.Logging
import scalaj.http._
import com.kdmforce.fusillimanager.impl.MessageSerializer._
import org.joda.time.DateTime
import scala.util.{Failure, Success, Try}
import scala.util.parsing.json.JSONObject



object Logger {

  def props = Props[Logger]

  private val BACKEND_KEY = sys.props.get("webappkey").getOrElse("nokey") //potrebbe creare problemi

  def backendLogger(logger:String, instance_id:Int, pipeline_id:Int, policies_id:String, status:String, message:String="", id: Int = 0, percentage:Int = 0): String = {
    val logMap = Map[String, Any](
      "instance_id"           -> instance_id,
      "pipeline_id"           -> pipeline_id,
      "policies_id"           -> policies_id,
      "version"               -> "0.2",
      "job_configuration_id"  -> id,
      "status"                -> status,
      "message"               -> message,
      "timestamp"             -> DateTime.now.getMillis(),
      "percentage"            -> percentage
    )
    val logWithHeader = Map[String, Any] (
      "key"       -> BACKEND_KEY,
      "log_data"  -> JSONObject(logMap)
    )

    var logJson : String = JSONObject(logWithHeader).toString()

    logJson
  }

  /*def backendLoggerDiscovery(logger:String, autodiscovery_id:Int, status:String, message:String="", id: Int = 0, percentage:Int = 0): String = {
    val logMap = Map[String, Any](
      "autodiscovery_id"      -> autodiscovery_id,
      "repo_id"               -> id,
      "status"                -> status,
      "message"               -> message,
      "timestamp"             -> DateTime.now.getMillis(),
      "percentage"            -> percentage
    )
    val logWithHeader = Map[String, Any] (
      "key"       -> BACKEND_KEY,
      "log_data"  -> JSONObject(logMap)
    )

    var logJson : String = JSONObject(logWithHeader).toString()

    logJson
  }*/

  class Logger extends Actor {

    private val log = Logging.getLogger(context.system, this)

    private var instances : Map[Int, LogInstanceData] = Map() //instances_id -> logData

    /*private var autodiscoveries : Map[Int, LogAutoDiscoveryData] = Map() //autodiscovery_id -> logData*/

    override def receive = {
      case NewInstanceLog(instances_id, data) =>
        log.info("New instance: {}", instances_id)
        instances = instances + (instances_id -> data)

      case DeleteInstanceLog(instances_id) =>
        log.info("Deleting instance: {}", instances_id)
        instances = instances - instances_id

      case LogMessage(instance: Int, job: JobMessage) =>
        val logger: String = (instances(instance)).logUrl
        val instance_id = instance
        val pipeline_id = (instances(instance)).pipeline_id
        val policies_id = (instances(instance)).policies_id
        val status = job.status
        val message = job.message
        val id = job.job_id
        val percentage = status match {
          case "END" => 100
          case _ => 0}

        val backendLoggerResult: String = backendLogger(logger, instance_id, pipeline_id, policies_id, status, message, id, percentage)

        logger match {
          case "" => log.info(s"Logged on stdout: ${backendLoggerResult}")
          case _ => {
            val logCall : Try[HttpResponse[String]] = Try {
              Http(s"${logger}").postData(backendLoggerResult).header("content-type", "application/json").asString.throwError
            }
            logCall match {
              case Success(_) =>
                log.info(s"Logged: ${backendLoggerResult}" )
              case Failure(e) =>
                log.error(s"Error while logging: ${e} \n Logger: ${backendLoggerResult}")
            }
          }
        }
        if (message == "END PIPELINE" || status == "ERROR") {self ! DeleteInstanceLog(instance)}

      /*case NewDiscoveryLog(autodiscovery_id, data) =>
        log.info("New autodiscovery: {}", autodiscovery_id)
        autodiscoveries = autodiscoveries + (autodiscovery_id -> data)

      case DeleteDiscoveryLog(autodiscovery_id) =>
        log.info("Deleting autodiscovery: {}", autodiscovery_id)
        autodiscoveries = autodiscoveries - autodiscovery_id

      case LogMessageDiscovery(autodiscovery: Int, job: JobMessage) =>
        val logger: String = (autodiscoveries(autodiscovery)).logUrl
        val autodiscovery_id = autodiscovery
        val status = job.status
        val message = job.message
        val id = job.job_id
        val percentage = status match {
          case "END" => 100
          case _ => 0}

        val backendLoggerDiscoveryResult: String = backendLoggerDiscovery(logger, autodiscovery_id, status, message, id, percentage)

        logger match {
          case "" => log.info(s"Logged on stdout: ${backendLoggerDiscoveryResult}")
          case _ => {
            val logCall : Try[HttpResponse[String]] = Try {
              Http(s"${logger}").postData(backendLoggerDiscoveryResult).header("content-type", "text/plain").header("Charset", "utf-8").asString.throwError
            }
            logCall match {
              case Success(_) =>
                log.info(s"Logged: ${backendLoggerDiscoveryResult}" )
              case Failure(e) =>
                log.error(s"Error while logging: ${e} \n Logger: ${backendLoggerDiscoveryResult}")
            }
          }
        }
        if (message == "END PIPELINE" || status == "ERROR") {self ! DeleteDiscoveryLog(autodiscovery)}*/
    }
  }
}


