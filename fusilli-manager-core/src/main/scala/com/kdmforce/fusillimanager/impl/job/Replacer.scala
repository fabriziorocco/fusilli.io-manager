package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{ConfigValue, Job}
import com.kdmforce.fusillimanager.impl.MessageSerializer.{JobMessage, LogMessage, PipelineAck, PipelineEnd, PipelineError, PipelineStart}

object Replacer {
  def props = Props[Replacer]
}

class Replacer(instance: Int, job: Job, follower: ActorRef ) extends Actor {
  var index = 0
  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case PipelineStart =>
      log.info("START REPLACER")
      logger ! (LogMessage(instance, JobMessage("START", "", job.job_configuration_id)))
      follower ! PipelineStart

    case PipelineEnd =>
      log.info("END REPLACER")
      logger ! (LogMessage(instance, JobMessage("END", "", job.job_configuration_id)))
      follower ! PipelineEnd
      self ! PoisonPill


    case PipelineError(ex) =>
      log.error(ex, "ERROR ON REPLACER")
      logger ! (LogMessage(instance, JobMessage("ERROR", ex.getMessage, job.job_configuration_id)))
      follower ! PipelineError(ex)
      self ! PoisonPill

    case el: Map[String, String] =>
      val result = replaceFun(el, job.job_configs)
      index = index + 1
      log.info("REPLACER OF " + s"${index}" + " ELEMENT")
      //logger ! (LogMessage(instance, JobMessage("REPLACER RESULT: " + s"${result}", "", job.job_configuration_id)))
      follower ! result
  }

  def replaceFun(doc: Map[String,String], jobConfigs: Seq[ConfigValue]): Map[String,String] = {
    var newDoc = doc
    for (j <- jobConfigs) {
      for (attr <- j.input_attrs) {
        val newVal = newDoc(attr).replace((j.config \ "replaced").as[String], (j.config \ "replacer").as[String])
        newDoc = newDoc - attr + (attr -> newVal)
      }
    }
    newDoc
  }
}
