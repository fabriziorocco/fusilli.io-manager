package com.kdmforce.fusillimanager.impl.connector.connection

import java.net.InetAddress
import java.nio.file._

import akka.stream.IOResult
import akka.stream.alpakka.ftp.scaladsl.Ftp
import akka.stream.alpakka.ftp.{FtpCredentials, FtpSettings}
import akka.stream.scaladsl.{FileIO,Flow,Sink}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import akka.stream.alpakka.file.scaladsl.Directory
import org.slf4j.{Logger, LoggerFactory}
import akka.stream.scaladsl.Source
import scala.concurrent.Future
import akka.util.ByteString

object FSDirectoryConnector extends Connector[ByteString]{

  private final val log: Logger = LoggerFactory.getLogger("FS DIRECTORY")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")

    val fs = FileSystems.getDefault

    log.info(s"DIRECTORY CONNECTION ${repo.schema_path.toString}")
    Directory
      .ls(fs.getPath(repo.schema_path))
      .flatMapConcat(path => {
        if(path.toString.endsWith(entity.entity_type)) {
          FileIO.fromPath(path).reduce((a, b) => a ++ b)
        } else {
          Source.empty
        }
      })
  }

  //da fare
  override def getSink(repo: Repo, entity: Entity) = {

    val file = Paths.get(repo.schema_path + entity.entity_name)

    FileIO.toPath(file)
    

  }



  

}

