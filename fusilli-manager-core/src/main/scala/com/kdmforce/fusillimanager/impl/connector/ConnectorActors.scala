package com.kdmforce.fusillimanager.impl.connector

import akka.stream.scaladsl.{Flow, Sink, Source}
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}

trait DataFile[X, Data] {

  def formatInput(entity: Entity) : Flow[X, Map[String, Data], AnyRef]
  def formatOutput(entity: Entity) : Flow[Map[String, Data], X, AnyRef]

}

trait Connector[X] {

  def getSource(repo:Repo, entity: Entity): Source[X, AnyRef]
  def getSink(repo:Repo, entity: Entity): Sink[X, AnyRef]

}