package com.kdmforce.fusillimanager.impl


import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Job


object Grahmizer{
def grahmize (jobList: Seq[Job] ):(List [Int], Map[Int, Int]) = {

    var unscheduled = Set[Int]()
    for (one <- jobList) unscheduled+=one.job_configuration_id


    var exiting = Map[Int,Int]()

    for (one <- jobList) {for(arc <-one.output_edges) {exiting+=(arc->one.job_configuration_id)}}


    var previouses = Map [Int, Set[Int]]()
      for(one<-jobList) previouses+= (one.job_configuration_id-> one.input_edges.map(exiting.get).toSet.flatten)


   var entering = Map[Int,Int]()
    for (one <- jobList) {for (arc <-one.input_edges){ entering+=(arc->one.job_configuration_id)}}

   var unscheduledFollowers = Map [Int, Set[Int]]()
     for(one<-jobList) unscheduledFollowers+= (one.job_configuration_id-> one.output_edges.map(entering.get).toSet.flatten)

    var execuTime = Map[Int,Int]()
    var revExecuTime = List[Int]()
    var firstOne:Int = FreeFirst(jobList, unscheduledFollowers)

    execuTime+=(firstOne->1)
    revExecuTime = firstOne :: revExecuTime
    unscheduled = unscheduled - firstOne

    for ( a <-previouses(firstOne)) unscheduledFollowers+=(a->(unscheduledFollowers(a)-firstOne))


    val size = jobList.length

    for (i<- 2 to size){
      val candidates : List[Job] = jobList.flatMap{ case x if (unscheduledFollowers(x.job_configuration_id).isEmpty && unscheduled.contains(x.job_configuration_id)) => Some(x)
                                                    case x => None}.toList
      val schedulant:Int = LeastLexOrd(candidates, entering, execuTime)
      unscheduled = unscheduled - schedulant

    for ( a <-previouses(schedulant)) unscheduledFollowers+=(a->(unscheduledFollowers(a)-schedulant))

      execuTime+=(schedulant->i)
      revExecuTime = schedulant :: revExecuTime

    }

    (revExecuTime,entering)

}



def FreeFirst (jobList: Seq[Job],unscheduledFollowers:Map [Int, Set[Int]]):Int = {
    val candidates : List[Int] = jobList.flatMap{ case x if unscheduledFollowers(x.job_configuration_id).isEmpty => Some(x.job_configuration_id)
                                                      case x => None}.toList
     candidates.head
  }





def LeastLexOrd(candidates:List[Job], entering: Map[Int, Int], execuTime:Map[Int,Int]):Int =  {
    var sigma = Map[Int, Set[Option[Int]]]();
    for (a<-candidates){
        var followSet = Set[Option[Int]]();
        for (b<-a.output_edges){ entering.get(b) match{ case Some (x)=> followSet+=Some(x)
                                                               case None =>     }

                           }
        sigma+=(a.job_configuration_id->followSet)
    }

    var varList = candidates.map(_.job_configuration_id)
    var minInt =varList.head;
    varList.drop(1)
    for (a<-varList) {minInt = compareSets(minInt, a , sigma(minInt).flatten, sigma(a).flatten)}


  minInt
}



def compareSets(ics:Int, ipsilon:Int, bigIcs:Set[Int], bigIpsilon:Set[Int]):Int = {
    ics match{
        case x if bigIcs.isEmpty => ics
        case x if bigIpsilon.isEmpty => ipsilon
        case x if (bigIcs.min<bigIpsilon.min) => ics
        case x if (bigIcs.min>bigIpsilon.min) => ipsilon
        case x => compareSets (ics, ipsilon, bigIcs-bigIcs.min, bigIpsilon-bigIpsilon.min)
    }

}


}
