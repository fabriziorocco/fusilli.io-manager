package com.kdmforce.fusillimanager.impl.connector

import java.nio.charset.StandardCharsets

import akka.NotUsed
import akka.stream.alpakka.csv.scaladsl.{CsvFormatting, CsvParsing, CsvToMap}
import akka.stream.scaladsl.{Flow, Source}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.EndpointSerializer.Entity
import com.kdmforce.fusillimanager.impl.connectors.DataFile


object CSV extends DataFile[ByteString, String] {
  override def formatInput(entity: Entity) = {

    val separator = entity.separator match {
      case "\\" => CsvParsing.Backslash
      case ":" => CsvParsing.Colon
      case "," => CsvParsing.Comma
      case "\"" => CsvParsing.DoubleQuote
      case ";" => CsvParsing.SemiColon
    }

    CsvParsing.lineScanner(separator)
      .via(CsvToMap.withHeadersAsStrings(StandardCharsets.UTF_8, entity.attributes.map(_.attr_name):_*))
      .drop(1)
  }

  override def formatOutput(entity: Entity) = {

    val header = entity.attributes.map(_.attr_name)
    Flow[Map[String, String]]
      .map(header.map)
      .prepend(Source.single(header))
    .via(CsvFormatting.format((entity.separator.toCharArray).apply(0)))

  }

}
