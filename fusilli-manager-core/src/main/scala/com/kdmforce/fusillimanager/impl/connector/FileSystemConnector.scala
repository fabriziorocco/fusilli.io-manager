package com.kdmforce.fusillimanager.impl.connector

import java.net.InetAddress
import java.nio.file.Paths

import akka.stream.IOResult
import akka.stream.alpakka.ftp.scaladsl.Ftp
import akka.stream.alpakka.ftp.{FtpCredentials, FtpSettings}
import akka.stream.scaladsl.FileIO
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.EndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.SftpConnector.spawnConnection
import com.kdmforce.fusillimanager.impl.connectors.Connector

import scala.concurrent.Future


object FileSystemConnector extends Connector[ByteString]{

  override def getSource(repo: Repo, entity: Entity) = {

    val file = Paths.get(repo.schema_path + entity.entity_name)

    FileIO.fromPath(file)

  }

  override def getSink(repo: Repo, entity: Entity) = {

    val file = Paths.get(repo.schema_path + entity.entity_name)

    FileIO.toPath(file)

  }
  
}

