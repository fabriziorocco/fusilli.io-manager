package com.kdmforce.fusillimanager.impl.discovery
import scalaj.http._
import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import com.kdmforce.fusillimanager.api.DiscoveryEndpointSerializer._
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.impl.MessageSerializer._
import scala.util.{Failure, Success, Try}

object RestTalker {
  def props = Props[RestTalker]
}
class RestTalker(autodiscovery: Int, job: DiscoveryDoc) extends Actor {

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {

    case PipelineStart =>
      log.info("Start RestTalker")
//      logger ! (LogMessageDiscovery(autodiscovery, JobMessage("START", "", job.repository.repo_id)))

    case PipelineEnd =>
      log.info("End RestTalker")
//      logger ! (LogMessageDiscovery(autodiscovery, JobMessage("END", "", job.repository.repo_id)))
      self ! PoisonPill

    case PipelineError(ex) =>
      log.error(ex, "ERROR on RestTalker")
//      logger ! (LogMessageDiscovery(autodiscovery, JobMessage("ERROR", ex.getMessage, job.repository.repo_id)))
      self ! PoisonPill

    case thingToPost: DiscoRepoOut =>

      Http(job.url_data_entry)
        .postData(jsonize(thingToPost))
        .header("content-type", "application/json").asString

  }

  def jsonize(disco :DiscoRepoOut) : String = {

    var arrayOfEntities = "["

    for (entity<-disco.entities) {
      arrayOfEntities = arrayOfEntities  + "{" + "\"entity_name\":" + "\"" + entity.entity_name + "\"" + ", \"entity_type\":" +
        "\"" + entity.entity_type + "\"" + ",\"separator\":" + "\"" + entity.separator + "\"" +
        ",\"attributes\":["

      for (attribute <-entity.attributes){
        arrayOfEntities = arrayOfEntities + "{" + "\"attr_name\":" + "\"" + attribute.attr_name + "\"" +
          ",\"attr_type\":" + "\"" + attribute.attr_type + "\"" +
          ",\"format\":" + "\"" + attribute.format + "\"" +
          ",\"length\":" + "\"" + attribute.length + "\"" + "},"
      }

      arrayOfEntities = arrayOfEntities.dropRight(1)+ "]},"
    }

    arrayOfEntities = arrayOfEntities.dropRight(1)+ "]"
    "{\"repo_id\":" + disco.repo_id + ", \"entities\":" + arrayOfEntities + "}"
  }
}