package com.kdmforce.fusillimanager.impl.connector

import java.net.InetAddress
import akka.stream.IOResult
import akka.stream.alpakka.ftp.scaladsl.Ftp
import akka.stream.alpakka.ftp.{FtpCredentials, FtpSettings}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.EndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connectors.Connector

import scala.concurrent.Future


object FtpConnector extends Connector[ByteString]{

  override def getSource(repo: Repo, entity: Entity) = {

    val settings = spawnConnection(repo)

    Ftp.fromPath(repo.schema_path + entity.entity_name, settings)
  }

  override def getSink(repo: Repo, entity: Entity) = {

    val settings = spawnConnection(repo)

    Ftp.toPath(repo.schema_path + entity.entity_name, settings)

  }


  def spawnConnection (repo: Repo): FtpSettings = {
    val credentials = FtpCredentials.create(repo.repo_user, repo.repo_password)

    FtpSettings
      .create(InetAddress.getByName(repo.host))
      .withPort(repo.port)
      .withCredentials(credentials)
      .withBinary(true)
      .withPassiveMode(true)
  }
}
