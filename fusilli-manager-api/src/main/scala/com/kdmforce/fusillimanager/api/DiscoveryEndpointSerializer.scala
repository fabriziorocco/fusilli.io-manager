package com.kdmforce.fusillimanager.api

import play.api.libs.json.{Format, Json, JsValue}

object DiscoveryEndpointSerializer {
  case class DiscoRepo(
                        repo_id : Int,
                        repo_name: String,
                        repo_user: String,
                        repo_password: String,
                        repo_type: String,
                        host: String,
                        schema_path: String,
                        port: Int
                      )

  object DiscoRepo{
    implicit val format: Format[DiscoRepo] = Json.format[DiscoRepo]
  }

  case class DiscoveryDoc(
                           url_data_entry:String,
                           autodiscovery_id:Int,
                           repository: DiscoRepo
                         )

  object DiscoveryDoc{
    implicit val format: Format[DiscoveryDoc] = Json.format[DiscoveryDoc]
  }


  case class DiscoAttribute(
                             attr_name:String,
                             attr_type: String,
                             format: String,
                             length: String
                        )

  object DiscoAttribute{
    implicit val format: Format[DiscoAttribute] = Json.format[DiscoAttribute]
  }

  case class DiscoEntity(
                          entity_name: String,
                          entity_type: String,
                          separator: String,
                          attributes: Seq[DiscoAttribute]
                         )

  object DiscoEntity{
    implicit val format: Format[DiscoEntity] = Json.format[DiscoEntity]
  }


  case class DiscoRepoOut(
                           repo_id:Int,
                           entities:Seq[DiscoEntity]
                         )

  object DiscoRepoOut{
    implicit val format: Format[DiscoRepoOut] = Json.format[DiscoRepoOut]
  }
}